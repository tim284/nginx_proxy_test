# Getting Started

```
git clone git@gitlab.com:tim284/nginx_proxy_test.git
cd nginx_proxy_test
docker-compose up -d
```

if successful:

`curl -i localhost:5000`

will produce:
```
➜  proxy_test git:(master) ✗ curl -i localhost:5000  
HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Content-Length: 51
Server: Werkzeug/1.0.1 Python/2.7.18
Date: Sat, 05 Jun 2021 14:35:22 GMT

This Compose/Flask demo has been viewed 13 time(s).% 
```

# Current Error
```
 curl -i localhost/flask
HTTP/1.1 502 Bad Gateway
Server: nginx/1.21.0
Date: Sat, 05 Jun 2021 14:36:56 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 157
Connection: keep-alive

<html>
<head><title>502 Bad Gateway</title></head>
<body>
<center><h1>502 Bad Gateway</h1></center>
<hr><center>nginx/1.21.0</center>
</body>
</html>
```
