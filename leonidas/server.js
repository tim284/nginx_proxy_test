'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('They say that the barbarian has come near and is comin\' on while we are wastin\' time. Truth, soon we shall either kill the barbarians, or else we are bound to be killed oursel\'s.');
});

app.listen(PORT, HOST);
console.log(`Yes, it is Running on http://${HOST}:${PORT}`);